1. Clone the project into your local system using the command git clone https://ramya_rao@bitbucket.org/ramya_rao/tys-integration-panel.git
2. Navigate to the my-app folder
3. Install all the required modules using the "npm install" command
4. Run the app using "npm start" command. 
5. The app will run on the browser at "localhost:3000"


This app is using Node Version = 8.15.0 and NPM Version = 6.4.1