import React from 'react';
import $ from 'jquery';

import './HelpText.css';

const HelpText = ({ dataToggle, helpText, placement }) => {
  $(() => {
    $('[data-toggle="tooltip"]').tooltip();
  });
  return (
    <span data-toggle={dataToggle} title={helpText} tabIndex="-1" data-placement={placement}>
      <i className="fa fa-question-circle ml-2" aria-hidden="true" />
    </span>
  );
};

export default HelpText;
