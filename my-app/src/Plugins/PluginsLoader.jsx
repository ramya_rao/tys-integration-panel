import React from 'react'
import Panel from '../Panel/Panel'
import socketIo from 'socket.io-client';
import { Observable } from 'rxjs';
import _ from 'lodash'

import EcovadisPlugin from './EcovadisPlugin'
import { RnRPlugin } from './RnRPlugin'

function usePartnerAgent({ url }) {
  const socket = React.useRef(socketIo.connect(url))

  function listen(event) {
    return new Observable(observer => {
      socket.current.on(event, data => {
        observer.next(data)
      })

      return observer
    })
  }

  // TYS_SOCKET_API_URL: "http://169.61.247.150:4000"
  function initSocket() {
    return listen("ibmuser@ibm.com")
  }

  function emit(data) {
    socket.current.emit('TYS_EVENT', data)
  }

  return {
    initSocket,
    emit,
    listen
  }
}


export default function PluginsLoader(props) {
  const { businessEntity } = props;

  const [partnerAgentData, setPartnerAgentData] = React.useState({})
  var argumentsFromPartner;

  function setArgumentsFromPartnerAgent(args) {
    argumentsFromPartner = args
  }

  //TODO:Please remove this hard coding and use from config.
  const partnerAgent = usePartnerAgent({ url: 'http://169.61.247.150:4000' })

  React.useEffect(() => {
    const subscription = partnerAgent.initSocket()
      .subscribe(
        res => {
          const {
            partner,
            Payload,
          } = res

          console.log(res)

          if (Payload) {
            setPartnerAgentData(previous => ({
              ...previous,
              [partner]: Payload
            }))
          } else {
            throw new Error("Response did not contain payload")
          }
        },
        error => {
          console.error(error)
        });
    
    var socketRequest = {
      partner: businessEntity.partner,
      action: argumentsFromPartner.action,
      user: "ibmuser@ibm.com",
      org: "org1",
      args: argumentsFromPartner.dunsNumber.toString() //businessEntity.dunsNumber
      // args: '294036723'
    }

    partnerAgent.emit(socketRequest);
    return () => subscription.unsubscribe();
  }, [])

  // if (Object.keys(partnerAgentData).length === 0) {
  //   return <Panel title="Loading Integrations..."></Panel>
  // }

  return (
    <>
      {/* ---------- TO BE DEVELOPED BY THE PARTNER ------------- */}
      <div className="row">
        <div className="col-lg-8">
          <Panel title="Sustainability - EcoVadis Rating">
            <EcovadisPlugin
              businessEntity={businessEntity}
              partnerData={partnerAgentData['ECOVADIS']}
              setArgumentsFromPartnerAgent={setArgumentsFromPartnerAgent}
            />
          </Panel>
        </div>
      </div>
    </>
  )
}
