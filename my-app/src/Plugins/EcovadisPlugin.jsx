import React, { useState, useEffect } from 'react';
import moment from 'moment'
import FIXTURES from './EcovadisData.json'
import _ from 'lodash'
const ECOVADIS_GREEN = "rgb(96,170,138)"

export function useEcovadisApi() {
    async function login() {
    }

    async function searchCompany(duns) {
        return {
            "search": {
                "code": 1,
                "message": "OK",
                "count": 1,
                "next-page": false
            },
            "companyIdentification": [
                {
                    "EVId": {
                        "value": "MI124918"
                    },
                    "_link": "https://data-sandbox.ecovadis-survey.com/companies/v0.1/company/MI124918"
                }
            ]
        }
    }

    async function getCompanyDetails(evid) {
        const details = FIXTURES[evid]

        return details
    }

    return {
        searchCompany,
        getCompanyDetails
    }
}

function Checkmark(props) {
    return (
        <span style={{ backgroundColor: props.color || "white", borderRadius: "50%", color: "white" }}>&#10003;</span>
    )
}

const EcovadisPlugin = props => {

    var companyDetails;
    if (props.partnerData !== undefined)
        companyDetails = props.partnerData

    if (companyDetails !== undefined) {
        var {
            hasRating,
            hasMedal,
            medalType,
            medalIcon,
            issueDate,
            expiryDate
        } = companyDetails.sustainableProcurementData.ecoVadisRating
    }

    const body = hasRating
        ? hasMedal
            ? `This company has an EcoVadis Rating, which measures the quality of a
            company's sustainability management system through its policies,
            actions, and results. In recognition of their achievement, this company
            has been awarded an EcoVadis Medal.`
            : `This company has an Ecovadis Rating, which measures the quality of a company's sustainability management
          system through its policies, actions, and results.`
        : `The EcoVadis Rating measures the quality of a company's sustainability
          management system through its policies, actions, and results.`

    const setArgs = () => {
        let socketRequestArguments = {
            action: "EcovadisGetCompanyDetails",
            dunsNumber: 294036723
        }
        props.setArgumentsFromPartnerAgent(socketRequestArguments)
    }

    useEffect(() => {
        setArgs();
    }, []);

    return (
        <div className="row p-1">
            <div className="col-9">
                <div className="card mt-0" style={{ border: "1px solid rgba(0, 0, 0, .125)" }}>
                    <div className="card-body">
                        {hasRating
                            ? <div className="card-title">
                                <h4 className="mb-0">Rated by EcoVadis <Checkmark color={ECOVADIS_GREEN} /></h4>
                                <small>Latest scorecard published on {moment(issueDate).format("DD MMMM YYYY")}</small>
                            </div>
                            : <h4 className="card-title">Not rated by EcoVadis <Checkmark /></h4>
                        }
                        <div className="row mt-3 mb-3">
                            {hasMedal && (
                                <div className="col-3 px-auto">
                                    <svg width="100" height="100">
                                        <image href={medalIcon.rasterUri} width="100" height="100"></image>
                                    </svg>
                                </div>)}
                            <div className="col">
                                <p>
                                    {body}
                                </p>
                                <RequestEcovadisButton />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="col">
                <h3>ecovadis</h3>
                <p>EcoVadis is the world's most trusted provider of business sustainability ratings
                  - and a strategic partner for companies that recognize the power of sustainability
                  to protect their brands, accelerate growth and strengthen customer loyalty and investor
                  confidence.
            </p>
                <a className="btn btn-text" href="https://ecovadis.com" target="_blank">&#8250;&nbsp;Learn more about EcoVadis</a>
            </div>
        </div>
    )
}

function RequestEcovadisButton() {
    return (
      <a 
        href="https://ecovadis.com"
        target="_blank"
        className="btn btn-success" 
        style={{ backgroundColor: ECOVADIS_GREEN, borderColor: ECOVADIS_GREEN }}>
        Request EcoVadis Rating
      </a>
    )
  }
  
export default EcovadisPlugin
