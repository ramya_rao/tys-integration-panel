import React from 'react'
import moment from 'moment'

import "./RnRPlugin.css"

import RnRData from './RnRData'
import HelpText from '../HelpText'
import RRLogo from './rr_logo.png'
import FHRScale from './fhr-scale.svg'

const FHR_HELP_TEXT = `The FHR is a number on a scale of 1 - 100 and is a measure of very high default risk in the short-term. 
  It is derived from Core Health and Resilience analysis, which incorporates a company's facility to meet ineternal and external obligations in the short-term.
  The Probability of Default, or EPD, is the value which express the likeliehood that this company will default as a percentage over the next one year.`

const RESILIENCE_HELP_TEXT = `Leverage is a solvency metric that depicts the extent to which a firm's assets are dependent on debt as compared to equity.
  Liquidity measures the ability of the firm to survive any short-term cirses that drain its asset reserves. 
  Earnings Performances is focused on profitability characteristics with respect to meeting internal and external obligations.`

export function RnRPlugin(props) {
  const {
    businessEntity,
    partnerData: data
  } = props

  if (!data || data.error) {
    return null
  }

  const {
    name,
    latestRatings,
    reportLinks,
  } = data

  function getRiskLevelLabel(fhr) {
    let riskLevel = undefined;

    if (fhr >= 0 && fhr < 20) {
      riskLevel = "Very High"
    } else if (fhr >= 20 && fhr < 40) {
      riskLevel = "High"
    } else if (fhr >= 40 && fhr < 60) {
      riskLevel = "Medium"
    } else if (fhr >= 60 && fhr < 80) {
      riskLevel = "Low"
    } else if (fhr >= 80 && fhr < 100) {
      riskLevel = "Very Low"
    } else {
      throw new Error("FHR out of bounds")
    }

    return riskLevel
  }

  if (latestRatings) {
    var latestRating = latestRatings[0]
  }

  const riskLevel = getRiskLevelLabel(latestRating.fhr)

  return (
    <div className="p-3">
      <div className="row justify-content-between">
        <div className="col">
          <h4>{name} FHR&reg;</h4>
        </div>
        <div className="col-auto">
          <img src={RRLogo} style={{ height: "75px" }} />
        </div>
      </div>
      <div className="row">
        <div className="col-2" style={{ maxWidth: "200px" }}>
          <FhrBadge
            fhr={latestRating.fhr}
            delta={latestRating.delta}
            eqyYear={latestRating.eqyYear}
            period={latestRating.period}
            financialDate={latestRating.financialDate}
          />
        </div>
        <div className="col text-left">
          <div className="row">
            <div className="col">
              <small className="text-secondary">{moment().format("MMMM DD, YYYY")}</small>
              <h4>
                <strong>Risk Level</strong>
                <div>{riskLevel} Default Risk</div>
              </h4>
            </div>
          </div>
          <br />
          <div className="row">
            <div className="col">
              <strong>
                FHR Metrics
                <span><HelpText dataToggle="tooltip" helpText={FHR_HELP_TEXT} placement="top" /></span>
              </strong>
              <FHRMetricsTable
                chs={latestRating.chs}
                delta={latestRating.delta}
                probabilityDefault={latestRating.probabilityDefault}
              />
            </div>
            <div className="col">
              <strong>
                Resilience Indicators: Short-Term Outlook
                <span><HelpText dataToggle="tooltip" helpText={RESILIENCE_HELP_TEXT} placement="top" /></span>
              </strong>
              <ResilienceIndicatorTable
                leverage={latestRating.leverage}
                liquidity={latestRating.liquidity}
                earningsPerformance={latestRating.earningsPerformance}
              />
            </div>
          </div>
        </div>
      </div>
      <br />
      <hr />
      <div className="row justify-content-between align-items-center">
        <div className="col">
          <FhrScale />
        </div>
        <div className="col-auto" >
          <span>
            <a className="btn btn-outline-secondary mr-2" href={require('./FHR-glossary.pdf')} target="_blank">FHR Data Glossary</a>
          </span>
          <span>
            <a className="btn btn-primary" href="https://fhr.rapidratings.com/tys?hs_preview=PbPXPOjH-13169064987" target="_blank">Request Sample Report</a>
          </span>
        </div>
      </div>
    </div>
  )
}

function Delta(props) {
  const {
    delta
  } = props

  return (
    <span className="d-flex flex-column" style={{ fontSize: "1em", lineHeight: "0.7em" }}>
      <span>{delta > 0 && <sup>&#8593;</sup>}</span>
      <span>{Math.abs(delta).toFixed(3)}</span>
      <span>{delta < 0 && <sub>&#8595;</sub>}</span>
    </span>
  )
}

function FhrBadge(props) {
  const {
    fhr,
    delta,
    financialDate,
    period,
    eqyYear,
  } = props

  let primaryColor = "red"
  let secondaryColor = "maroon"

  if (fhr <= 20) {
    primaryColor = "red"
    secondaryColor = "darkred"
  } else if (fhr > 20 && fhr <= 40) {
    primaryColor = "orange"
    secondaryColor = "darkorange"
  } else if (fhr > 40 && fhr <= 60) {
    primaryColor = "gold"
    secondaryColor = "goldenrod"
  } else if (fhr > 60 && fhr <= 80) {
    primaryColor = "green"
    secondaryColor = "darkgreen"
  } else if (fhr > 80 && fhr <= 100) {
    primaryColor = "blue"
    secondaryColor = "darkblue"
  } else {
    throw new Error("FHR out of range")
  }

  return (
    <div style={{ height: "100%" }}>
      <div style={{ backgroundColor: primaryColor, height: "70%" }} className="text-light d-flex flex-column justify-content-center text-center">
        <div className="d-flex justify-content-center align-items-center" style={{ fontSize: "1.2rem" }}>
          <span className="h1 mr-2">{fhr}</span>
          <span><Delta delta={delta} /></span>
        </div>
        <h3><strong>FHR</strong>&reg;</h3>
      </div>
      <div style={{ backgroundColor: secondaryColor, height: "30%" }} className="text-light text-center p-2">
        <div>{moment(financialDate).format("MMMM DD, YYYY")}</div>
        <div>(fiscal {period} {eqyYear})</div>
      </div>
    </div>
  )
}

function FHRMetricsTable(props) {
  const {
    chs,
    delta,
    probabilityDefault
  } = props

  let coreHealthScoreLabel;

  if (chs >= 0 && chs < 20) {
    coreHealthScoreLabel = "Very Poor"
  } else if (chs >= 20 && chs < 40) {
    coreHealthScoreLabel = "Poor"
  } else if (chs >= 40 && chs < 60) {
    coreHealthScoreLabel = "Medium"
  } else if (chs >= 60 && chs < 80) {
    coreHealthScoreLabel = "Strong"
  } else if (chs >= 80 && chs < 100) {
    coreHealthScoreLabel = "Very Strong"
  } else {
    throw new Error("CHS out of range")
  }

  return (
    <table className="table table-sm" style={{ borderTop: "0px" }}>
      <thead>
        <tr style={{ borderTop: "2px solid white" }}>
          <th className="pl-0">Core Health</th>
          <th className="border-left pl-2">Annual Delta</th>
          <th className="border-left pl-2">Probability of Default</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td className="pl-0">{coreHealthScoreLabel}</td>
          <td className="border-left pl-2">{delta.toFixed(3)}%</td>
          <td className="border-left pl-2">{probabilityDefault.toFixed(1)}%</td>
        </tr>
      </tbody>
    </table>
  )
}

function ResilienceIndicatorTable(props) {
  let {
    leverage,
    liquidity,
    earningsPerformance
  } = props

  if (leverage === "Average") {
    leverage = "Adequate"
  }

  if (liquidity === "Average") {
    liquidity = "Adequate"
  }

  if (earningsPerformance === "Average") {
    earningsPerformance = "Adequate"
  }

  return (
    <table className="table table-sm" style={{ borderTop: "0px" }}>
      <thead>
        <tr style={{ borderTop: "2px solid white" }}>
          <th className="pl-0">Leverage</th>
          <th className="border-left pl-2">Liquidity</th>
          <th className="border-left pl-2">Earnings Performance</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td className="pl-0">{leverage}</td>
          <td className="border-left pl-2">{liquidity}</td>
          <td className="border-left pl-2">{earningsPerformance}</td>
        </tr>
      </tbody>
    </table>
  )
}

function FhrScale() {
  return (
    <div>
      <img src={FHRScale} height="65px" />
      {/* <table>
        <tr style={{ height: "8px" }}>
          <td style={{ backgroundColor: "red", minWidth: "150px"}}></td>
          <td style={{ backgroundColor: "orange", minWidth: "150px"}}></td>
          <td style={{ backgroundColor: "yellow", minWidth: "150px"}}></td>
          <td style={{ backgroundColor: "green", minWidth: "150px"}}></td>
          <td style={{ backgroundColor: "blue", minWidth: "150px"}}></td>
        </tr>
        <tr className="text-left">
          <td><strong>Very High Risk</strong></td>
          <td><strong>High Risk</strong></td>
          <td><strong>Medium Risk</strong></td>
          <td><strong>Low Risk</strong></td>
          <td><strong>Very Low Risk</strong></td>
        </tr>
        <tr className="text-left">
          <td>0-20</td>
          <td>21-40</td>
          <td>41-60</td>
          <td>61-80</td>
          <td>81-100</td>
        </tr>
      </table> */}
    </div>
  )
}