import React from 'react';
import logo from './logo.svg';
import './App.css';
import PluginsLoader from '../src/Plugins/PluginsLoader'

function App() {
  let businessEntity = {
    partnersData: [
      {
        partner: "ECOVADIS",
        appId: "1"
      }
    ]
  }

  let pluginLoaderList = businessEntity.partnersData.map(function(partnerData){
    return <PluginsLoader businessEntity = {partnerData} />
  })

  return (
    <div className="container-fluid">
      {pluginLoaderList}
    </div>
  );
}

export default App;
