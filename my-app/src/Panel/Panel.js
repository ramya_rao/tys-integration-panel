import React, { useState } from 'react';
import './Panel.css';

const Panel = ({
  title, actionButton, children, isButtonEnabled, initiallyCollapsed,
}) => {
  const ActionButton = actionButton;
  const [isCollapsed, setIsCollapsed] = useState(initiallyCollapsed);

  return (
    <div className="reading-pane-panel shadow-sm mt-3 p-2" style={{ maxHeight: `${isCollapsed ? '50px' : '100%'}` }}>
      <div className="reading-pane-header mb-2">
        <div className="d-flex justify-content-between align-items-center">
          <h6 className="m-0 p-0">
            <strong>
              {title}
            </strong>
          </h6>
          <div>
            {actionButton && isButtonEnabled && (
              <ActionButton />
            )}
            <button type="button" className="btn btn-sm text-muted btn-light ml-1" onClick={() => setIsCollapsed(!isCollapsed)}>
              {isCollapsed ? (
                <i className="fa fa-chevron-down" aria-hidden="true" />
              ) : (
                <i className="fa fa-chevron-up" aria-hidden="true" />
              )}
            </button>
          </div>
        </div>
      </div>
      {!isCollapsed && children}
    </div>
  );
};

export default Panel;
