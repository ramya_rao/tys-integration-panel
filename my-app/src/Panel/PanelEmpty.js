import React from 'react';

const PanelEmpty = ({ label }) => (
  <div className="col d-flex justify-content-center">
    <p className="text-muted text-center">
      {label}
    </p>
  </div>
);

export default PanelEmpty;
